create table profiles
(
    id           serial not null
        constraint profiles_pk
            primary key,
    first_name   text   not null,
    last_name    text,
    email        text,
    main_image   text,
    profile_info text,
    last_session timestamp
);

comment on table profiles is 'main profile data objects';

create table credentials
(
    id         serial not null
        constraint credentials_pk
            primary key,
    profile_id integer
        constraint credentials_profiles_id_fk
            references profiles
            on update cascade,
    login      text   not null,
    password   text   not null
);

create unique index credentials_login_uindex
    on credentials (login);

create table user_sessions
(
    id          serial    not null
        constraint user_sessions_pk
            primary key,
    profile_id  integer
        constraint user_sessions_profiles_id_fk
            references profiles
            on update cascade,
    start_date  timestamp not null,
    expire_date timestamp not null,
    last_update timestamp
);

create table markups
(
    id              serial    not null
        constraint markups_pk
            primary key,
    from_profile_id integer
        constraint markups_uf_fk
            references profiles,
    to_profile_id   integer
        constraint markups_ut_fk
            references profiles,
    date_added      timestamp not null,
    m_type          text
);

create unique index markups_p_index
    on markups (from_profile_id, to_profile_id);

create table chats
(
    id             serial not null
        constraint chats_pk
            primary key,
    date_start     timestamp,
    chat_profile_1 integer
        constraint chats_p1_fk
            references profiles,
    chat_profile_2 integer
        constraint chats_p2_fk
            references profiles
);


create unique index chats_chat_profile_1_chat_profile_2_uindex
    on chats (chat_profile_1, chat_profile_2);

create table messages
(
    id           serial not null
        constraint messages_pk
            primary key,
    date_added   timestamp,
    profile_id   integer
        constraint messages_p_fk
            references profiles,
    chat_id      integer
        constraint messages_ch_fk
            references chats,
    message_text text
);


