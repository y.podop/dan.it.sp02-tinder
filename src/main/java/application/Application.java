package application;

import application.server.DatabasePool;
import application.server.ServletDispatcher;
import application.server.classes.ScanType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        try {
            //load application properties
            InputStream props = DatabasePool.class.getClassLoader().getResourceAsStream("config/app.properties");
            Properties appProps = new Properties();
            appProps.load(props);
            //update schema
            DatabasePool.migrateSchema();
            //configure and start http server
            new ServletDispatcher
                    .Builder()
                    .configureTemplateEngine()
                    .configureRoutes(ScanType.AUTO)
                    .configureFilters(ScanType.AUTO)
                    .start(HerokuEnv.port());
            // Custom controllers and filters could be added here

        } catch (SQLException e) {
            logger.error("DB connection error.", e);
        } catch (Exception e) {
            logger.error("Servlet dispatcher start error.", e);
        }
    }
}
