package application.sp02.classes;

public enum MarkupTarget {
    PROFILE, POST, IMAGE
}
