package application.sp02.classes;

public enum OrderDirection {
    ASC, DESC
}
