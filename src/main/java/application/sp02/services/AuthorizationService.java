package application.sp02.services;

import application.server.classes.MiddlewareService;
import application.sp02.database.dao.ProfileDao;
import application.sp02.database.dao.UserSessionDao;
import application.sp02.entities.Credentials;
import application.sp02.entities.Profile;
import application.sp02.entities.UserSession;

import java.time.LocalDateTime;
import java.util.Optional;

public class AuthorizationService extends MiddlewareService {
    private final ProfileDao profileDao = new ProfileDao();
    private final UserSessionDao userSessionDao = new UserSessionDao();

    public Optional<Long> authorizeUser(Credentials credentials, int sessionLength) {

        return profileDao
                .login(credentials)
                .map(u -> {
                    UserSession sess = userSessionDao
                            .getUserSession(u)
                            .orElse(new UserSession());
                    sess.setProfileId(u);
                    sess.setSessionStart(LocalDateTime.now());
                    sess.setSessionExpire(LocalDateTime.now().plusSeconds(sessionLength));
                    sess.setSessionLastUpdate(LocalDateTime.now());
                    userSessionDao.save(sess).getId();
                    Profile profile = profileDao.getById(Profile.class, u).orElse(new Profile());
                    profile.setLastSession(LocalDateTime.now());
                    profileDao.save(profile);
                    return sess.getId();
                });

    }

    public boolean logout(Long sessionId) {
        return userSessionDao.deleteById(UserSession.class, sessionId);
    }

    public boolean checkUserSession(Long sessionId) {
        return userSessionDao
                .getById(UserSession.class, sessionId)
                .filter(session -> session.getSessionExpire().isAfter(LocalDateTime.now()))
                .isPresent();
    }
}
