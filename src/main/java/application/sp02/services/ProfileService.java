package application.sp02.services;

import application.server.classes.MiddlewareService;
import application.sp02.classes.MarkupType;
import application.sp02.classes.OrderDirection;
import application.sp02.database.dao.MarkupDao;
import application.sp02.database.dao.MessageDao;
import application.sp02.database.dao.ProfileDao;
import application.sp02.database.dao.UserSessionDao;
import application.sp02.entities.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ProfileService extends MiddlewareService {
    private final ProfileDao profileDao = new ProfileDao();
    private final MarkupDao markupDao = new MarkupDao();
    private final UserSessionDao sessionDao = new UserSessionDao();
    private final MessageDao messageDao = new MessageDao();
    private final AuthorizationService authorizationService = new AuthorizationService();
    private final ChatService chatService = new ChatService();

    public Optional<List<Profile>> getProfiles() {
        return profileDao.getAll(Profile.class);
    }

    public Optional<List<Profile>> getLikedProfiles(Long sessionId) {
        return profileDao.getLiked(getSessionOwner(sessionId).map(s -> s.getId()).orElse(-1L), MarkupType.LIKE);
    }

    public Optional<Profile> getProfile(Long id) {
        return profileDao.getById(Profile.class, id);
    }

    public Optional<Profile> getSessionOwner(Long sessionId) {
        Optional<UserSession> session = sessionDao.getById(UserSession.class, sessionId);
        return session.flatMap(sess -> profileDao.getById(Profile.class, sess.getProfileId()));
    }

    public Map<String, Integer> getProfileStats(Long profileId) {
        return profileDao.profileStats(profileId);
    }

    public Optional<MarkupType> getOwnerProfileStats(Long ownerProfileId, Long profileId) {
        return profileDao.ownerProfileStats(ownerProfileId, profileId);
    }

    public void addProfileMarkup(Long fromProfileId, Long toProfileId, MarkupType markupType) {
        markupDao.deleteAllProfileFromMarkup(fromProfileId, toProfileId);
        Markup markup = new Markup();
        markup.setFromId(fromProfileId);
        markup.setToId(toProfileId);
        markup.setMarkupDate(LocalDateTime.now());
        markup.setMarkupType(markupType.name());
        Optional.ofNullable(markupDao.save(markup))
                .filter(m -> m.getMarkupType().equals(MarkupType.LIKE.name()))
                .ifPresent(m -> chatService.addProfileChat(fromProfileId, toProfileId));
    }

    public boolean checkSession(Long sessionId) {
        return authorizationService.checkUserSession(sessionId);
    }

    public Optional<List<Profile>> getProfilesPage(int pageNum, int pageSize, String orderBy, OrderDirection orderDirection) {
        return Optional.ofNullable(profileDao.getPage(Profile.class, pageNum, pageSize, orderBy, orderDirection));
    }

    public Optional<Chat> getProfileChat(Long from, Long to) {
        return chatService.getProfileChatWithMessages(from, to);
    }

    public void addChatMessage(Long from, Long to, String messageText) {
        chatService.getProfileChat(from, to).ifPresent((c) -> {
            Message m = new Message();
            m.setChatId(c.getId());
            m.setMessageDate(LocalDateTime.now());
            m.setOwnerId(from);
            m.setText(messageText);
            messageDao.save(m);
        });
    }

    public Optional<List<Profile>> getOwnerMarkups(Long ownerId, MarkupType markupType) {
        return profileDao.getOwnerMarkups(ownerId, markupType);
    }
}
