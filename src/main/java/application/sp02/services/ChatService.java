package application.sp02.services;

import application.server.classes.MiddlewareService;
import application.sp02.database.dao.ChatDao;
import application.sp02.database.dao.MessageDao;
import application.sp02.entities.Chat;
import application.sp02.entities.Message;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ChatService extends MiddlewareService {
    private final ChatDao chatDao = new ChatDao();
    private final MessageDao messageDao = new MessageDao();

    public List<Message> getChatMessages(Long chatId) {
        try {
            return messageDao.getByField(Message.class, "chatId", chatId);
        } catch (NoSuchFieldException e) {
            return Collections.emptyList();
        }
    }

    public Optional<Chat> addProfileChat(Long from, Long to) {
        Chat chat = new Chat();
        chat.setDateStart(LocalDateTime.now());
        chat.setChatProfile1(from);
        chat.setChatProfile2(to);
        return Optional.ofNullable(chatDao.getProfileChat(from, to)
                .orElseGet(() -> chatDao.save(chat)));
    }

    public Optional<Chat> getProfileChatWithMessages(Long from, Long to) {
        return chatDao
                .getProfileChat(from, to)
                .map(chat -> {
                    chat.setMessages(getChatMessages(chat.getId()));
                    return chat;
                });

    }

    public Optional<Chat> getProfileChat(Long from, Long to) {
        return chatDao
                .getProfileChat(from, to);
    }
}
