package application.sp02.services;

import application.server.classes.MiddlewareService;
import application.server.utils.PasswordHashUtil;
import application.sp02.database.dao.CredentialsDao;
import application.sp02.database.dao.ProfileDao;
import application.sp02.database.dao.UserSessionDao;
import application.sp02.entities.Profile;
import application.sp02.entities.UserSession;

import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.Optional;

public class RegistrationService extends MiddlewareService {
    private final ProfileDao profileDao = new ProfileDao();
    private final CredentialsDao credentialsDao = new CredentialsDao();
    private final UserSessionDao userSessionDao = new UserSessionDao();

    public Optional<Long> registerProfile(Profile profile) {
        if (!credentialsDao.checkCredentialsPresent(profile.getEmail())
                && Objects.nonNull(profileDao.save(profile).getId())) {
            profile.getCredentials().setProfileId(profile.getId());
            try {

                profile.getCredentials().setPassword(PasswordHashUtil.passwordHash(profile.getCredentials().getPassword()));
                if (Objects.isNull(credentialsDao.save(profile.getCredentials()).getId())) {
                    profileDao.deleteById(Profile.class, profile.getId());
                    return Optional.empty();
                }
            } catch (NoSuchAlgorithmException e) {
                return Optional.empty();
            }
        }
        return Optional.ofNullable(profile.getId());
    }


    public Optional<Long> authorizeWhenRegistered(UserSession userSession) {
        return Optional.ofNullable(userSessionDao.save(userSession).getId());
    }
}
