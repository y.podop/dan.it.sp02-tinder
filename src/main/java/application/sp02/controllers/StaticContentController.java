package application.sp02.controllers;

import application.server.annotations.WebAppController;
import application.server.utils.WebControllerUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;

@WebAppController(urlPattern = {"/assets/type/img/name/*",
        "/assets/type/script/name/*",
        "/assets/type/css/name/*"}, pathVariables = {"type", "name"})
public class StaticContentController extends HttpServlet {
    private final String assetsFolder = "templates/sp02";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String url = req.getRequestURI();
        Map<String, String> p = WebControllerUtils.parsePathVars(this.getClass().getAnnotation(WebAppController.class).pathVariables(), url);
        if(Objects.nonNull(p.get("type"))&&Objects.nonNull(p.get("name"))){
            Path path = Paths.get(assetsFolder, "/",p.get("type"),"/", p.get("name"));
            try (OutputStream os = resp.getOutputStream()) {
                Files.copy(path, os);
            }
        }
    }
}
