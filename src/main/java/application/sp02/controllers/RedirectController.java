package application.sp02.controllers;


import application.server.annotations.WebAppController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebAppController(urlPattern = "/*")
public class RedirectController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse rs) throws ServletException, IOException {
        rs.sendRedirect("/index");
    }
}
