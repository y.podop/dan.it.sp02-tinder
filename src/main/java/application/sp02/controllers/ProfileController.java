package application.sp02.controllers;

import application.server.TemplateEngine;
import application.server.annotations.TemplateEngineConf;
import application.server.annotations.WebAppController;
import application.server.annotations.WebControllerService;
import application.server.classes.HttpServiceLocator;
import application.server.classes.MiddlewareService;
import application.server.utils.CookieUtil;
import application.server.utils.WebControllerUtils;
import application.sp02.MessagesStaticContent;
import application.sp02.classes.MarkupType;
import application.sp02.entities.Chat;
import application.sp02.entities.Profile;
import application.sp02.services.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebAppController(urlPattern = {"/profile/*"}, pathVariables = {"id", "action", "list"})
@TemplateEngineConf(defaultTemplate = "pages/profile.ftl")
public class ProfileController extends HttpServiceLocator {
    @WebControllerService
    private final ProfileService profileService;
    private final TemplateEngine templateEngine;
    private final String defaultTemplateName;

    public ProfileController(TemplateEngine templateEngine, MiddlewareService middlewareService) {
        super(templateEngine, middlewareService);
        this.profileService = (ProfileService) middlewareService;
        this.templateEngine = templateEngine;
        this.defaultTemplateName = this.getClass().getAnnotation(TemplateEngineConf.class).defaultTemplate();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Map<String, Object> templateData = new HashMap<>();
        doRequest(req, resp, templateData);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String> p = WebControllerUtils
                .parsePathVars(this.getClass().getAnnotation(WebAppController.class).pathVariables(), req.getRequestURI());
        Long sid = CookieUtil.read(req, CookieUtil.USER_SESSION_ID)
                .map(c -> Long.valueOf(c.getValue()))
                .orElse(-1L);
        Map<String, Object> templateData = new HashMap<>();
        Profile sessionOwner = profileService
                .getSessionOwner(sid)
                .orElse(new Profile());
        if (Objects.nonNull(req.getParameter("markup")) && Objects.nonNull(p.get("id"))) {
            profileService.addProfileMarkup(sessionOwner.getId(),
                    Long.valueOf(p.get("id")),
                    MarkupType.valueOf(req.getParameter("markup")));
        } else if (Objects.nonNull(req.getParameter("messageText")) && Objects.nonNull(p.get("id"))) {
            profileService.addChatMessage(sessionOwner.getId(),
                    Long.valueOf(p.get("id")),
                    req.getParameter("messageText"));
        }
        doRequest(req, resp, templateData);
    }

    private void doRequest(HttpServletRequest req, HttpServletResponse resp, Map<String, Object> templateData) {
        String url = req.getRequestURI();
        Map<String, String> p = WebControllerUtils
                .parsePathVars(this.getClass().getAnnotation(WebAppController.class).pathVariables(), url);

        Long sid = CookieUtil.read(req, CookieUtil.USER_SESSION_ID)
                .map(c -> Long.valueOf(c.getValue()))
                .orElse(-1L);
        Profile sessionOwner = profileService
                .getSessionOwner(sid)
                .orElse(new Profile());
        templateData.put("profileOwner", false);
        if (p.get("list").equals("all")) {
            templateData.put("pageContent", "profileList");
            List<Profile> all = profileService
                    .getProfiles()
                    .orElse(new ArrayList<>());
            List<Profile> liked = profileService
                    .getLikedProfiles(sid)
                    .orElse(new ArrayList<>());
            all.removeIf(pr -> liked.contains(pr));
            all.removeIf(pr -> pr.getId().equals(sessionOwner.getId()));
            liked.removeIf(pr -> pr.getId().equals(sessionOwner.getId()));
            templateData.put("profiles", all);
            templateData.put("likedProfiles", liked);
        } else {
            templateData.put("pageContent", "profileSingle");
            Profile profile = p.get("id").equals("owner") ?
                    sessionOwner
                    : profileService
                    .getProfile(Long.valueOf(p.get("id")))
                    .orElse(new Profile());

            templateData.put("profileOwner", profile.getId().equals(sessionOwner.getId()));
            templateData.put("profile", profile);
            templateData.put("profileStats", profileService.getProfileStats(profile.getId()));
            templateData.put("ownerProfileMarkup", profileService
                    .getOwnerProfileStats(sessionOwner.getId(), profile.getId())
                    .map(ps -> ps.name())
                    .orElse(""));
            if (!(profile.getId().equals(sessionOwner.getId()))) {
                templateData.put("chat", profileService
                        .getProfileChat(sessionOwner.getId(), profile.getId()).orElse(new Chat()));
            } else {
                templateData.put("whoLikedMe", profileService
                        .getOwnerMarkups(sessionOwner.getId(), MarkupType.LIKE).orElse(new ArrayList<>()));
            }
        }

        templateData.put("profileUserLogged", profileService.checkSession(sid));
        templateData.put("pageName", "SP02-Tinder. Profile page");
        templateEngine.render(defaultTemplateName, templateData, resp);
    }

}
