package application.sp02.controllers;

import application.server.TemplateEngine;
import application.server.annotations.TemplateEngineConf;
import application.server.annotations.WebAppController;
import application.server.annotations.WebControllerService;
import application.server.classes.HttpServiceLocator;
import application.server.classes.MiddlewareService;
import application.server.utils.CookieUtil;
import application.sp02.MessagesStaticContent;
import application.sp02.database.dao.UserSessionDao;
import application.sp02.entities.Credentials;
import application.sp02.entities.Profile;
import application.sp02.entities.UserSession;
import application.sp02.services.RegistrationService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@WebAppController(urlPattern = {"/registration"})
@TemplateEngineConf(defaultTemplate = "pages/registration.ftl")
public class RegistrationController extends HttpServiceLocator {
    @WebControllerService
    private final RegistrationService registrationService;
    private final TemplateEngine templateEngine;
    private final String defaultTemplateName;

    public RegistrationController(TemplateEngine templateEngine, MiddlewareService middlewareService) {
        super(templateEngine, middlewareService);
        this.templateEngine = templateEngine;
        this.registrationService = (RegistrationService) middlewareService;
        this.defaultTemplateName = this.getClass().getAnnotation(TemplateEngineConf.class).defaultTemplate();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("pageName", "SP02-Tinder. Registration form");
        String returnUrl = Objects.nonNull(req.getParameter("redirectTo")) ? req.getParameter("redirectTo") : "/index";
        templateData.put("returnUrl", returnUrl);
        templateEngine.render(defaultTemplateName, templateData, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Profile profile = new Profile();
        profile.setFirstName(req.getParameter("firstName"));
        profile.setLastName(req.getParameter("lastName"));
        profile.setEmail(req.getParameter("email"));
        profile.setProfileImage(req.getParameter("profileImage"));
        profile.setProfileInfo(req.getParameter("profileInfo"));
        Credentials credentials = new Credentials();
        credentials.setLogin(profile.getEmail());
        credentials.setPassword(req.getParameter("password"));
        profile.setCredentials(credentials);
        profile.setLastSession(LocalDateTime.now());


        Map<String, Object> templateData = new HashMap<>();
        String returnUrl = Objects.nonNull(req.getParameter("redirectTo")) ? req.getParameter("redirectTo") : "/index";
        templateData.put("returnUrl", returnUrl);
        templateData.put("pageName", "SP02-Tinder. Registration form");
        registrationService.registerProfile(profile).ifPresent((pId) -> {
            registrationService
                    .authorizeWhenRegistered(UserSessionDao.createNewSession(pId))
                    .ifPresent(sessionId -> {
                        Cookie cookieUser = new Cookie(CookieUtil.USER_SESSION_ID, sessionId.toString());
                        cookieUser.setPath("/");
                        cookieUser.setDomain("");
                        cookieUser.setMaxAge(CookieUtil.TIME_REGULAR);
                        resp.addCookie(cookieUser);
                        try {
                            resp.sendRedirect(returnUrl);
                        } catch (IOException e) {
                            //
                        }
                    });
        });
        templateData.put("error", MessagesStaticContent.REGISTRATION_ERROR);
        templateEngine.render(defaultTemplateName, templateData, resp);
    }
}
