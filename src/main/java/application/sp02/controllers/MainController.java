package application.sp02.controllers;

import application.server.TemplateEngine;
import application.server.annotations.TemplateEngineConf;
import application.server.annotations.WebAppController;
import application.server.annotations.WebControllerService;
import application.server.classes.HttpServiceLocator;
import application.server.classes.MiddlewareService;
import application.server.utils.CookieUtil;
import application.server.utils.WebControllerUtils;
import application.sp02.MessagesStaticContent;
import application.sp02.classes.OrderDirection;
import application.sp02.services.AuthorizationService;
import application.sp02.services.ProfileService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@WebAppController(urlPattern = "/index")
@TemplateEngineConf(defaultTemplate = "pages/index.ftl")
public class MainController extends HttpServiceLocator {
    @WebControllerService
    private final ProfileService profileService;
    private final TemplateEngine templateEngine;
    private final String defaultTemplateName;

    public MainController(TemplateEngine templateEngine, MiddlewareService middlewareService) {
        super(templateEngine, middlewareService);
        this.templateEngine = templateEngine;
        this.profileService = (ProfileService) middlewareService;
        this.defaultTemplateName = this.getClass().getAnnotation(TemplateEngineConf.class).defaultTemplate();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map<String, Object> templateData = new HashMap<>();
        templateData.put("profileUserLogged", CookieUtil.checkIfUserLoggedIn(req));
        templateData.put(MessagesStaticContent.PAGE_NAME_FIELD, MessagesStaticContent.PAGE_NAME_MAIN);
        templateData.put("lastRegistered", profileService
                .getProfilesPage(1, 4, "id", OrderDirection.DESC)
                .orElse(new ArrayList<>()));
        templateEngine.render(defaultTemplateName, templateData, resp);
    }
}
