package application.sp02.controllers;

import application.server.TemplateEngine;
import application.server.annotations.TemplateEngineConf;
import application.server.annotations.WebAppController;
import application.server.annotations.WebControllerService;
import application.server.classes.HttpServiceLocator;
import application.server.classes.MiddlewareService;
import application.server.utils.CookieUtil;
import application.server.utils.WebControllerUtils;
import application.sp02.MessagesStaticContent;
import application.sp02.entities.Credentials;
import application.sp02.services.AuthorizationService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@WebAppController(urlPattern = {"/login/*"}, pathVariables = {"action"})
@TemplateEngineConf(defaultTemplate = "pages/login.ftl")
public class LoginController extends HttpServiceLocator {

    @WebControllerService
    private final AuthorizationService authorizationService;
    private final TemplateEngine templateEngine;
    private final String defaultTemplateName;

    public LoginController(TemplateEngine templateEngine, MiddlewareService middlewareService) {
        super(templateEngine, middlewareService);
        this.templateEngine = templateEngine;
        this.authorizationService = (AuthorizationService) middlewareService;
        this.defaultTemplateName = this.getClass().getAnnotation(TemplateEngineConf.class).defaultTemplate();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("pageName", "SP02-Tinder. Login form");
        String returnUrl = Objects.nonNull(req.getParameter("redirectTo")) ? req.getParameter("redirectTo") : "/index";
        templateData.put("returnUrl", returnUrl);
        templateEngine.render(defaultTemplateName, templateData, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String url = req.getRequestURI();
        Map<String, String> p = WebControllerUtils.parsePathVars(this.getClass().getAnnotation(WebAppController.class).pathVariables(), url);
        Map<String, Object> templateData = new HashMap<>();
        switch (p.get("action")) {
            case "authorize":
                templateData.put(MessagesStaticContent.PAGE_NAME_FIELD, MessagesStaticContent.PAGE_NAME_LOGIN);
                Credentials credentials = new Credentials(
                        req.getParameter("login"),
                        req.getParameter("password")
                );
                String returnUrl = Objects.nonNull(req.getParameter("redirectTo")) ? req.getParameter("redirectTo") : "/index";
                authorizationService
                        .authorizeUser(credentials, Boolean.valueOf(req.getParameter("keepLoggedIn")) ? CookieUtil.TIME_REMEMBER : CookieUtil.TIME_REGULAR)
                        .ifPresentOrElse(sessionId -> {
                            Cookie cookieUser = new Cookie(CookieUtil.USER_SESSION_ID, sessionId.toString());
                            cookieUser.setPath("/");
                            cookieUser.setDomain("");
                            cookieUser.setMaxAge(Boolean.valueOf(req.getParameter("keepLoggedIn")) ? CookieUtil.TIME_REMEMBER : CookieUtil.TIME_REGULAR);
                            resp.addCookie(cookieUser);
                            try {
                                resp.sendRedirect(returnUrl);
                            } catch (IOException e) {
                                templateEngine.render(defaultTemplateName, templateData, resp);
                            }

                        }, () -> {
                            templateData.put("returnUrl", returnUrl);
                            templateData.put("error", MessagesStaticContent.LOGIN_ERROR);
                            templateEngine.render(defaultTemplateName, templateData, resp);
                        });
                break;
            case "logout":
                templateData.put(MessagesStaticContent.PAGE_NAME_FIELD, MessagesStaticContent.PAGE_NAME_LOGIN);
                Optional<Cookie> cookieOptional = CookieUtil.read(req, CookieUtil.USER_SESSION_ID);
                Cookie uid = cookieOptional.orElse(new Cookie(CookieUtil.USER_SESSION_ID, ""));
                if (authorizationService.logout(Long.valueOf(uid.getValue()))) {
                    uid.setPath("/");
                    uid.setDomain("");
                    uid.setMaxAge(0);
                    resp.addCookie(uid);
                    resp.sendRedirect("/index");
                }
                break;
            default:
                templateEngine.render(defaultTemplateName, templateData, resp);
                break;
        }
    }
}
