package application.sp02.controllers;

import application.server.TemplateEngine;
import application.server.annotations.TemplateEngineConf;
import application.server.annotations.WebAppController;
import application.server.annotations.WebControllerService;
import application.server.classes.HttpServiceLocator;
import application.server.classes.MiddlewareService;
import application.sp02.MessagesStaticContent;
import application.sp02.services.ErrorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@WebAppController(urlPattern = {"/error"})
@TemplateEngineConf(defaultTemplate = "pages/error.ftl")
public class ErrorController extends HttpServiceLocator {
    @WebControllerService
    private final ErrorService errorService;
    private final TemplateEngine templateEngine;
    private final String defaultTemplateName;

    public ErrorController(TemplateEngine templateEngine, MiddlewareService middlewareService) {
        super(templateEngine, middlewareService);
        this.templateEngine = templateEngine;
        this.errorService = (ErrorService) middlewareService;
        this.defaultTemplateName = this.getClass().getAnnotation(TemplateEngineConf.class).defaultTemplate();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String returnUrl = Objects.nonNull(req.getParameter("redirectTo")) ? req.getParameter("redirectTo") : "/index";
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("profileUserLogged", false);
        templateData.put("warn", String.format(MessagesStaticContent.PROFILE_WARN, returnUrl, returnUrl));
        templateData.put(MessagesStaticContent.PAGE_NAME_FIELD, MessagesStaticContent.PAGE_NAME_ERROR);
        templateEngine.render(defaultTemplateName,templateData,resp);
    }
}
