package application.sp02;

public class MessagesStaticContent {
    private MessagesStaticContent(){}
    public static String LOGIN_ERROR = "Wrong login or password.";
    public static String REGISTRATION_ERROR = "Error when registration.";
    public static String PROFILE_WARN = "У Вас нет прав на просмотр страницы. " +
            " Пожалуйста, <a href='/registration?redirectTo=%s'>зарегистрируйтесь</a> " +
            " или <a href='/login?redirectTo=%s'>ввойдите</a> в свой аккаунт";
    public static String PAGE_NAME_FIELD = "pageName";
    public static String PAGE_NAME_LOGIN = "SP02-Tinder. Login form";
    public static String PAGE_NAME_ERROR = "SP02-Tinder. Error page";
    public static String PAGE_NAME_MAIN = "SP02-Tinder";

}
