package application.sp02.database.dao;

import application.server.DatabasePool;
import application.sp02.database.queries.MarkupQueries;
import application.sp02.entities.Markup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MarkupDao extends AbstractDao<Markup>{
    private final Logger logger = LoggerFactory.getLogger(ProfileDao.class);
    public void deleteAllProfileFromMarkup(Long from, Long to){
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(MarkupQueries.DELETE_PROFILE_FROM_MARKUP)) {
            stmt.setLong(1, from);
            stmt.setLong(2, to);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }
}
