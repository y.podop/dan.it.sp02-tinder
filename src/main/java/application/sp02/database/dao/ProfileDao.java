package application.sp02.database.dao;

import application.server.DatabasePool;
import application.server.utils.PasswordHashUtil;
import application.server.utils.jdbc.QueryUtil;
import application.sp02.classes.MarkupType;
import application.sp02.database.queries.ProfileQueries;
import application.sp02.database.queries.UserQueries;
import application.sp02.entities.Credentials;
import application.sp02.entities.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class ProfileDao extends AbstractDao<Profile> {
    private final Logger logger = LoggerFactory.getLogger(ProfileDao.class);
    private final QueryUtil<Profile> queryUtil = new QueryUtil<>();

    public ProfileDao() {
        super();
    }

    public Optional<Long> login(Credentials credentials) {
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(UserQueries.GET_USERID_BY_CREDENTIALS)) {
            stmt.setString(1, credentials.getLogin());
            stmt.setString(2, PasswordHashUtil.passwordHash(credentials.getPassword()));
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next())
                    return Optional.ofNullable(rs.getLong(1));
                else
                    return Optional.empty();
            }
        } catch (SQLException | NoSuchAlgorithmException e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

    public Optional<List<Profile>> getLiked(Long userId, MarkupType markupType) {
        List<Profile> profileList = new ArrayList<>();
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(UserQueries.GET_LIKED)) {
            stmt.setString(1, markupType.name());
            stmt.setLong(2, userId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Profile profile = queryUtil.initializeFields(new Profile(), rs);
                    profileList.add(profile);
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
        return Optional.of(profileList);
    }

    public Map<String, Integer> profileStats(Long profileId) {
        Map<String, Integer> profileStatsMap = new HashMap<>();
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(UserQueries.GET_STATS)) {
            stmt.setLong(1, profileId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    profileStatsMap.put(rs.getString(2),rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return profileStatsMap;
    }
    public Optional<MarkupType> ownerProfileStats(Long ownerProfileId, Long profileId) {
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(UserQueries.GET_OWNER_PROFILE_STATS)) {
            stmt.setLong(1, ownerProfileId);
            stmt.setLong(2, profileId);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    return Optional.ofNullable(MarkupType.valueOf(rs.getString(2)));
                }
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return Optional.empty();
    }
    public Optional<List<Profile>> getOwnerMarkups(Long ownerId, MarkupType markupType){
        List<Profile> profileList = new ArrayList<>();
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(ProfileQueries.OWNER_MARKUPS_PROFILES)) {
            statement.setLong(1,ownerId);
            statement.setString(2,markupType.name());
            statement.executeQuery();
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                Profile profile = queryUtil.initializeFields(new Profile(),rs);
                profileList.add(profile);
            }
        } catch (SQLException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, throwables.getMessage()), throwables);
        }
        return Optional.of(profileList);
    }

}
