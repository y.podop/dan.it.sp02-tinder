package application.sp02.database.dao;

import application.server.DatabasePool;
import application.sp02.database.queries.UserQueries;
import application.sp02.entities.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CredentialsDao extends AbstractDao<Credentials> {
    private final Logger logger = LoggerFactory.getLogger(CredentialsDao.class);

    public CredentialsDao() {
        super();
    }

    public boolean checkCredentialsPresent(String email) {
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(UserQueries.GET_USERID_BY_CREDENTIALS)) {
            stmt.setString(1, email);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next())
                    return rs.getLong(1) > 0;
                else
                    return false;
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
            return false;
        }
    }
}
