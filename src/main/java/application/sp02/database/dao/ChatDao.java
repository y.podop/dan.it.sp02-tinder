package application.sp02.database.dao;

import application.server.DatabasePool;
import application.server.utils.jdbc.QueryUtil;
import application.sp02.database.queries.ChatQueries;
import application.sp02.entities.Chat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class ChatDao extends AbstractDao<Chat> {
    private final Logger logger = LoggerFactory.getLogger(ChatDao.class);
    private final QueryUtil<Chat> queryUtil = new QueryUtil<>();

    public Optional<Chat> getProfileChat(Long from, Long to) {
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement stmt = connection.prepareStatement(ChatQueries.GET_PROFILE_CHAT)) {
            stmt.setLong(1, from);
            stmt.setLong(2, to);
            stmt.setLong(3, to);
            stmt.setLong(4, from);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return Optional.ofNullable((Chat) queryUtil.initializeFields(new Chat(), rs));
                }
            }catch (SQLException e){
                logger.error(e.getMessage());
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return Optional.empty();
    }
}
