package application.sp02.database.dao;

import application.server.DatabasePool;
import application.server.utils.CookieUtil;
import application.server.utils.jdbc.QueryUtil;
import application.sp02.database.queries.SessionQueries;
import application.sp02.entities.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;

public class UserSessionDao extends AbstractDao<UserSession> {
    private final Logger logger = LoggerFactory.getLogger(UserSessionDao.class);
    private final QueryUtil<UserSession> queryUtil = new QueryUtil<>();
    public UserSessionDao() {
        super();
    }

    public Optional<UserSession> getUserSession(Long profileId) {
        String query = SessionQueries.GET_SESSION_ID_BY_USER;
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setLong(1, profileId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                UserSession session = new UserSession();
                session = queryUtil.initializeFields(session, resultSet);
                return Optional.ofNullable(session);
            }
        } catch (SQLException throwables) {
            logger.error(String.format("Session fetching error: %s", throwables.getMessage()));

        }
        return Optional.empty();
    }


    public static UserSession createNewSession(Long profileId) {
        UserSession userSession = new UserSession();
        userSession.setProfileId(profileId);
        userSession.setSessionExpire(LocalDateTime.now().plusSeconds(CookieUtil.TIME_REGULAR));
        userSession.setSessionStart(LocalDateTime.now());
        userSession.setSessionLastUpdate(LocalDateTime.now());
        return userSession;
    }
}
