package application.sp02.database.dao;

import application.sp02.classes.OrderDirection;

import java.util.List;
import java.util.Optional;

public interface Dao<T> {
    Optional<List<T>> getAll(Class<T> clazz);

    Optional<T> getById(Class<T> clazz, Long id);

    List<T> getByField(Class<T> clazz, String fieldName, Object fieldValue) throws NoSuchFieldException;

    List<T> getPage(Class<T> clazz, int pageNum, int pageSize, String orderBy, OrderDirection orderDirection);

    T save(T t);

    boolean delete(Class<T> clazz, T t);

    boolean deleteById(Class<T> clazz, Long id);
}
