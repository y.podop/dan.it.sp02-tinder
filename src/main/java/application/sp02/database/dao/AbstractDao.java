package application.sp02.database.dao;

import application.server.DatabasePool;
import application.server.annotations.DataColumn;
import application.server.utils.jdbc.QueryUtil;
import application.sp02.classes.MarkupType;
import application.sp02.classes.OrderDirection;
import application.sp02.database.queries.ProfileQueries;
import application.sp02.entities.AbstractEntity;
import application.sp02.entities.Profile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public abstract class AbstractDao<T extends AbstractEntity> implements Dao<T> {
    private final Logger logger = LoggerFactory.getLogger(AbstractDao.class);
    private final QueryUtil<T> queryUtil = new QueryUtil<>();

    @Override
    public Optional<List<T>> getAll(Class<T> clazz) {
        String query = queryUtil.getAllQuery(clazz);
        List<T> objectsList = new ArrayList<>();
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                T dataObject = clazz.getConstructor().newInstance();
                queryUtil.initializeFields(dataObject, resultSet);
                objectsList.add(dataObject);
            }
        } catch (SQLException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, query), throwables);
        }
        return Optional.of(objectsList);
    }

    @Override
    public Optional<T> getById(Class<T> clazz, Long id) {
        String query = queryUtil.getByIdQuery(clazz, id);
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                T dataObject = clazz.getConstructor().newInstance();
                queryUtil.initializeFields(dataObject, resultSet);
                return Optional.of(dataObject);
            }
        } catch (SQLException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, query), throwables);
        }
        return Optional.empty();
    }

    @Override
    public List<T> getByField(Class<T> clazz, String fieldName, Object fieldValue) throws NoSuchFieldException {
        List<T> objectsList = new ArrayList<>();
        String query = queryUtil.getByFieldEqualsQuery(clazz,
                clazz.getDeclaredField(fieldName).getAnnotation(DataColumn.class).name(),
                fieldValue);
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                T dataObject = clazz.getConstructor().newInstance();
                queryUtil.initializeFields(dataObject, rs);
                objectsList.add(dataObject);
            }
        } catch (SQLException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, query), throwables);
        }
        return objectsList;
    }

    @Override
    public List<T> getPage(Class<T> clazz, int pageNum, int pageSize, String orderBy, OrderDirection orderDirection) {
        String query = queryUtil.getPageQuery(clazz, pageNum, pageSize, orderBy, orderDirection);
        List<T> objectsList = new ArrayList<>();
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                T dataObject = clazz.getConstructor().newInstance();
                queryUtil.initializeFields(dataObject, rs);
                objectsList.add(dataObject);
            }
        } catch (SQLException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, query), throwables);
        }
        return objectsList;
    }

    @Override
    public T save(T t) {
        String query = Objects.isNull(t.getId()) ?
                queryUtil.insertQuery(t) :
                queryUtil.updateQuery(t);

        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            queryUtil.fillStatement(statement, t);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                t.setId(rs.getLong(1));
            }
        } catch (SQLException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, query), throwables);
        }
        return t;
    }

    @Override
    public boolean delete(Class<T> clazz, T t) {
        return deleteById(clazz, t.getId());
    }

    @Override
    public boolean deleteById(Class<T> clazz, Long id) {
        if (Objects.isNull(id)) return false;
        String query = queryUtil.deleteByIdQuery(clazz, id);
        try (Connection connection = DatabasePool.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            logger.error(String.format(QueryUtil.QUERY_ERROR, query), throwables);
            return false;
        }
    }


}
