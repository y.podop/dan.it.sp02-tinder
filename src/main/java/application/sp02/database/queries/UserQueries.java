package application.sp02.database.queries;

public class UserQueries {
    public static final String GET_USERID_BY_CREDENTIALS = "select p.id from credentials c" +
            " left join profiles p on c.profile_id=p.id" +
            " where c.login=? and c.password=?";

    public static final String GET_LIKED = "select * from profiles p" +
            " left join markups m on m.to_profile_id=p.id" +
            " where " +
            " m.m_type=? " +
            " and m.from_profile_id=?";

    public static final String GET_STATS = "select count(*),m_type from markups where to_profile_id=? group by 2";
    public static final String GET_OWNER_PROFILE_STATS = "select count(*),m_type from markups where from_profile_id=? and to_profile_id=? group by 2";
    public static final String CHANGE_PASSWORD = "update credentials set password=? where id=?";
    public static final String CHECK_PROFILES_BY_LOGIN = "select count(*) from credentials where login=?";
}
