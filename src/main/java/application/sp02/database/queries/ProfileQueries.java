package application.sp02.database.queries;

public class ProfileQueries {
 public static final String OWNER_MARKUPS_PROFILES="select * from  profiles p" +
         " left join markups m on p.id = m.from_profile_id" +
         " where m.to_profile_id=?" +
         " and m.m_type=?";
}
