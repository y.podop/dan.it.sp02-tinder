package application.sp02.database.queries;

public class ChatQueries {
    public static String GET_PROFILE_CHAT = "select * " +
            " from chats " +
            " where " +
            " (chat_profile_1 = ? and chat_profile_2 = ?) " +
            " or (chat_profile_1 = ? and chat_profile_2 = ?)" +
            " limit 1 offset 0";
    public static String DELETE_PROFILE_CHAT = "delete " +
            " from chats " +
            " where " +
            " (chat_profile_1 = ? and chat_profile_2 = ?) " +
            " or (chat_profile_1 = ? and chat_profile_2 = ?)";

}
