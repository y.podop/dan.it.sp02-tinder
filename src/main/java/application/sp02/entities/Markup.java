package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;
import application.sp02.classes.MarkupTarget;
import application.sp02.classes.MarkupType;

import java.time.LocalDateTime;

@DataTable(name = "markups")
public class Markup extends AbstractEntity{
    @DataColumn(name="from_profile_id")
    private Long fromId;
    @DataColumn(name="to_profile_id")
    private Long toId;
    @DataColumn(name="date_added")
    private LocalDateTime markupDate;
    @DataColumn(name="m_type")
    private String markupType;


    public Long getFromId() {
        return fromId;
    }

    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    public Long getToId() {
        return toId;
    }

    public void setToId(Long toId) {
        this.toId = toId;
    }

    public LocalDateTime getMarkupDate() {
        return markupDate;
    }

    public void setMarkupDate(LocalDateTime markupDate) {
        this.markupDate = markupDate;
    }

    public String getMarkupType() {
        return markupType;
    }

    public void setMarkupType(String markupType) {
        this.markupType = markupType;
    }
}
