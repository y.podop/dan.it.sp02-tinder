package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;

import java.time.LocalDateTime;

@DataTable(name = "profiles")
public class Profile extends AbstractEntity{

    @DataColumn(name = "first_name")
    private String firstName;
    @DataColumn(name = "last_name")
    private String lastName;
    @DataColumn(name = "email")
    private String email;
    @DataColumn(name = "profile_info")
    private String profileInfo;
    @DataColumn(name = "main_image")
    private String profileImage;
    @DataColumn(name = "last_session")
    private LocalDateTime lastSession;

    private Credentials credentials;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getProfileInfo() {
        return profileInfo;
    }

    public void setProfileInfo(String profileInfo) {
        this.profileInfo = profileInfo;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public LocalDateTime getLastSession() {
        return lastSession;
    }

    public void setLastSession(LocalDateTime lastSession) {
        this.lastSession = lastSession;
    }
}
