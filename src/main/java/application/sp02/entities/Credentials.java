package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;

@DataTable(name = "credentials")
public class Credentials extends AbstractEntity{

    @DataColumn(name="login")
    private String login;
    @DataColumn(name="password")
    private String password;
    @DataColumn(name="profile_id")
    private Long profileId;

    public Credentials() {
    }

    public Credentials(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }
}

