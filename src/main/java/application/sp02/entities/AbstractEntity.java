package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.EntityId;

public class AbstractEntity {
    @EntityId
    @DataColumn(name="id")
    private Long id;

    public AbstractEntity() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractEntity)) return false;

        AbstractEntity that = (AbstractEntity) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
