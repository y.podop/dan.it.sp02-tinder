package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;

import java.time.LocalDateTime;
import java.util.List;

@DataTable(name = "chats")
public class Chat extends AbstractEntity {

    @DataColumn(name = "date_start")
    private LocalDateTime dateStart;

    @DataColumn(name = "chat_profile_1")
    private Long chatProfile1;

    @DataColumn(name = "chat_profile_2")
    private Long chatProfile2;

    private List<Message> messages;

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
    }

    public Long getChatProfile1() {
        return chatProfile1;
    }

    public void setChatProfile1(Long chatProfile1) {
        this.chatProfile1 = chatProfile1;
    }

    public Long getChatProfile2() {
        return chatProfile2;
    }

    public void setChatProfile2(Long chatProfile2) {
        this.chatProfile2 = chatProfile2;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

}
