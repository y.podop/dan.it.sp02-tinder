package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;

import java.time.LocalDateTime;

@DataTable(name = "messages")
public class Message extends AbstractEntity {

    @DataColumn(name = "date_added")
    private LocalDateTime messageDate;
    @DataColumn(name = "profile_id")
    private Long ownerId;
    @DataColumn(name = "chat_id")
    private Long chatId;
    @DataColumn(name = "message_text")
    private String text;


    public LocalDateTime getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(LocalDateTime messageDate) {
        this.messageDate = messageDate;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }
}
