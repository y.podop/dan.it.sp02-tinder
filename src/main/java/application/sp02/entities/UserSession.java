package application.sp02.entities;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;

import java.time.LocalDateTime;

@DataTable(name="user_sessions")
public class UserSession extends AbstractEntity{

    @DataColumn(name="profile_id")
    private Long profileId;
    @DataColumn(name="start_date")
    private LocalDateTime sessionStart;
    @DataColumn(name="expire_date")
    private LocalDateTime sessionExpire;
    @DataColumn(name="last_update")
    private LocalDateTime sessionLastUpdate;


    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public LocalDateTime getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(LocalDateTime sessionStart) {
        this.sessionStart = sessionStart;
    }

    public LocalDateTime getSessionExpire() {
        return sessionExpire;
    }

    public void setSessionExpire(LocalDateTime sessionExpire) {
        this.sessionExpire = sessionExpire;
    }

    public LocalDateTime getSessionLastUpdate() {
        return sessionLastUpdate;
    }

    public void setSessionLastUpdate(LocalDateTime sessionLastUpdate) {
        this.sessionLastUpdate = sessionLastUpdate;
    }
}
