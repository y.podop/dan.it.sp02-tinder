package application.sp02.filters;

import application.server.annotations.WebAppFilter;
import application.server.utils.CookieUtil;
import application.sp02.services.AuthorizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebAppFilter(urlPatterns = {"/chat/*", "/profile/id/*","/profile/action/*"}, order = 1)
public class AuthenticationFilter implements HttpFilter {
    private final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    private final AuthorizationService authorizationService = new AuthorizationService();


    @Override
    public void doHttpFilter(HttpServletRequest rq, HttpServletResponse rs, FilterChain filterChain) throws ServletException, IOException {
        logger.info(String.format("Authentication filtering logger triggered on path: %s", ((HttpServletRequest) rq).getRequestURI()));
        CookieUtil
                .read(rq, CookieUtil.USER_SESSION_ID)
                .ifPresentOrElse((session) -> {
                    if (authorizationService.checkUserSession(Long.valueOf(session.getValue()))) {
                        try {
                            filterChain.doFilter(rq, rs);
                        } catch (IOException | ServletException e) {
                            logger.error("Auth filtering error");
                        }
                    } else {
                        Cookie uid = new Cookie(CookieUtil.USER_SESSION_ID, "");
                        uid.setPath("/");
                        uid.setDomain("");
                        uid.setMaxAge(0);
                        rs.addCookie(uid);
                        try {
                            ((HttpServletResponse) rs).sendRedirect("/error?redirectTo=" +rq.getRequestURI());
                        } catch (IOException e) {
                            logger.error("Redirect error");
                        }
                    }
                }, () -> {
                    try {
                        rs.sendRedirect("/error?redirectTo=" + rq.getRequestURI());
                    } catch (IOException e) {
                        logger.error("Redirect error");
                    }
                });
    }


}
