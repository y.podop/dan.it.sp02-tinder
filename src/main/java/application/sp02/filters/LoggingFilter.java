package application.sp02.filters;

import application.server.annotations.WebAppFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebAppFilter(urlPatterns = {"/*"}, order = -1)
public class LoggingFilter implements HttpFilter {
    private final Logger logger = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    public void doHttpFilter(HttpServletRequest servletRequest, HttpServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info(String.format("LoggingFilter  logger triggered on path: %s", servletRequest.getRequestURI()));
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
