package application.server.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

public class CookieUtil {
    public static final String USER_ID = "UID";
    public static final String USER_SESSION_ID = "USER_SESSION_ID";
    public static final int TIME_REMEMBER = 2_592_000;
    public static final int TIME_REGULAR = 86_400;

    private CookieUtil() {
    }

    private static boolean validate(Cookie c) {
        return true;
    }

    public static Optional<Cookie> read(HttpServletRequest rq, String name) {
        return Optional.ofNullable(rq.getCookies())
                .flatMap(x ->
                        Arrays.stream(x).filter(c -> c.getName().equals(name)).findAny()
                );
    }

    public static boolean checkIfUserLoggedIn(HttpServletRequest rq) {
        return CookieUtil
                .read(rq, CookieUtil.USER_SESSION_ID)
                .filter(p->Long.valueOf(p.getValue())>0)
                .isPresent();
    }
}
