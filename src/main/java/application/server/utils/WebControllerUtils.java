package application.server.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebControllerUtils {
    private WebControllerUtils(){}
    public static Map<String, String> parsePathVars(String[] vars, String uri){
        String[] param = uri.split("\\/");
        List<String> p = Arrays.asList(param);
        Map<String, String> varMap = new HashMap<>();
        Arrays.stream(vars).forEach(v->varMap.put(v, p.get(p.indexOf(v)+1)));
        return varMap;
    }
}
