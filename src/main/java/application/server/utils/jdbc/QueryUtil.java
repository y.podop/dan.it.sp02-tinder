package application.server.utils.jdbc;

import application.server.annotations.DataColumn;
import application.server.annotations.DataTable;
import application.server.annotations.EntityId;
import application.sp02.classes.OrderDirection;
import application.sp02.entities.AbstractEntity;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class QueryUtil<T extends AbstractEntity> implements IQueryUtil<T> {
    public static final String QUERY_ERROR="query execution error:  %s";

    private Optional<Field> getKeyField() {
        return Arrays.stream(AbstractEntity.class.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(EntityId.class)).findFirst();
    }

    private String getDataTable(Class<T> clazz) {
        return clazz.getAnnotation(DataTable.class).name();
    }

    @Override
    public String getAllQuery(Class<T> clazz) {
        String keyField = getKeyField()
                .map(field -> field.getName())
                .orElse("id");
        return String.format("select * from %s order by %s desc", getDataTable(clazz), keyField);
    }

    @Override
    public String getPageQuery(Class<T> clazz, int pageNum, int pageSize, String orderBy, OrderDirection orderDirection) {
        int offset = (pageNum>0)?pageSize*(--pageNum):0;
        return String.format("select * from %s order by %s %s limit %d offset %d",
                getDataTable(clazz),
                orderBy,
                orderDirection.name(),
                pageSize,
                offset);
    }

    @Override
    public String getByIdQuery(Class<T> clazz, Long id) {
        String pattern = "select * from %s where %s=%d";
        return getKeyField()
                .map(field -> String.format(pattern, getDataTable(clazz), field.getName(), id))
                .orElseGet(() -> String.format(pattern, getDataTable(clazz), "id", id));
    }

    @Override
    public String getByFieldEqualsQuery(Class<T> clazz, String fieldName, Object fieldValue) {
        String pattern = "select * from %s where %s=%s";
        return String.format(pattern, getDataTable(clazz), fieldName, fieldValue);
    }

    @Override
    public String insertQuery(T t) {
        List<String> fields = Arrays
                .stream(t.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(DataColumn.class))
                .map(field -> field.getAnnotation(DataColumn.class).name())
                .collect(Collectors.toList());
        String valuesParametersPattern = StringUtils.repeat("?,", Math.toIntExact(fields.size()));
        String keyField = getKeyField()
                .map(field -> field.getName())
                .orElse("id");
        String pattern = "insert into %s (%s) values(%s) returning %s";
        String fieldsPattern = String.join(",", fields);
        return String.format(pattern,
                getDataTable((Class<T>) t.getClass()),
                fieldsPattern,
                valuesParametersPattern.substring(0, valuesParametersPattern.length() - 1),
                keyField
        );
    }

    @Override
    public String updateQuery(T t) {
        List<String> fields = Arrays
                .stream(t.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(DataColumn.class))
                .map(field -> field.getAnnotation(DataColumn.class).name())
                .collect(Collectors.toList());
        String valueParameters = String.join(",", fields.stream()
                .map(field -> String.format("%s=?", field))
                .collect(Collectors.toList()));
        String keyField = getKeyField()
                .map(field -> field.getName())
                .orElse("id");
        String pattern = "update %s set %s where %s=%d returning %d";

        return String.format(pattern,
                getDataTable((Class<T>) t.getClass()),
                valueParameters,
                keyField,
                t.getId(),
                t.getId()
        );
    }

    @Override
    public String deleteByIdQuery(Class<T> clazz, Long id) {
        String keyField = getKeyField()
                .map(field -> field.getName())
                .orElse("id");
        return String.format("delete from %s where %s=%d", getDataTable(clazz), keyField, id);
    }

    @Override
    public T initializeFields(T dataObject, ResultSet resultSet) {
        String keyField = getKeyField()
                .map(field -> field.getName())
                .orElse("id");
        try {
            dataObject.setId(resultSet.getLong(keyField));
        } catch (SQLException throwables) {
            //
        }
        Arrays.stream(dataObject.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(DataColumn.class))
                .forEach(field -> {
                    field.setAccessible(true);
                    String typeName = field.getType().getName();
                    try {
                        switch (typeName) {
                            case "java.lang.String":
                                String fString = resultSet.getString(field.getAnnotation(DataColumn.class).name());
                                field.set(dataObject, fString);
                                break;
                            case "java.lang.Integer":
                                Integer fInt = resultSet.getInt(field.getAnnotation(DataColumn.class).name());
                                field.set(dataObject, fInt);
                                break;
                            case "java.lang.Long":
                                Long fLong = resultSet.getLong(field.getAnnotation(DataColumn.class).name());
                                field.set(dataObject, fLong);
                                break;
                            case "java.time.LocalDateTime":
                                LocalDateTime fdate = resultSet
                                        .getTimestamp(field.getAnnotation(DataColumn.class).name())
                                        .toInstant()
                                        .atZone(ZoneId.systemDefault())
                                        .toLocalDateTime();
                                field.set(dataObject, fdate);
                                break;
                            default:
                                break;
                        }
                    } catch (SQLException | IllegalAccessException throwables) {
                        //
                    }

                });
        return dataObject;
    }

    @Override
    public PreparedStatement fillStatement(PreparedStatement statement, T t) {
        AtomicInteger counter = new AtomicInteger(0);
        Arrays.stream(t.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(DataColumn.class))
                .forEach(field -> {
                    field.setAccessible(true);
                    String typeName = field.getType().getName();
                    try {
                        switch (typeName) {
                            case "java.lang.String":
                                statement.setString(counter.incrementAndGet(), (String) field.get(t));
                                break;
                            case "java.lang.Integer":
                                statement.setInt(counter.incrementAndGet(), (Integer) field.get(t));
                                break;
                            case "java.lang.Long":
                                statement.setLong(counter.incrementAndGet(), (Long) field.get(t));
                                break;
                            case "java.time.LocalDateTime":
                                statement.setTimestamp(counter.incrementAndGet(), java.sql.Timestamp.valueOf((LocalDateTime) field.get(t)));
                                break;
                            default:
                                break;
                        }
                    } catch (SQLException | IllegalAccessException throwables) {
                        //
                    }

                });
        return statement;
    }
}
