package application.server.utils.jdbc;

import java.util.Arrays;

public class QueryPredicateUtil {


    public static String equals(String tableName, String columnName, String value) {
        return String.format(" and %s.%s = %s", tableName, columnName, value);
    }

    public static String like(String tableName, String columnName, String value) {
        return String.format(" and %s.%s like %s", tableName, columnName, value);
    }

    public static String greaterOrEqual(String tableName, String columnName, String value) {
        return String.format(" and %s.%s >= %s", tableName, columnName, value);
    }

    public static String lessOrEqual(String tableName, String columnName, String value) {
        return String.format(" and %s.%s <= %s", tableName, columnName, value);
    }

    public static String in(String tableName, String columnName, String[] values) {
        return String.format(" and %s.%s in (%s)", tableName, columnName, String.join(",", Arrays.asList(values)));
    }

    public static String and(String[] predicates) {
        return String.join(" and ", Arrays.asList(predicates));
    }

    public static String or(String[] predicates) {
        return String.join(" or ", Arrays.asList(predicates));
    }
}
