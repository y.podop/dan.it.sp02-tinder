package application.server.utils.jdbc;

import application.sp02.classes.OrderDirection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public interface IQueryUtil<T> {

    String getAllQuery(Class<T> clazz);
    String getPageQuery(Class<T> clazz, int pageNum, int pageSize, String orderBy, OrderDirection orderDirection);
    String getByIdQuery(Class<T> clazz, Long id);
    String getByFieldEqualsQuery(Class<T> clazz, String fieldName, Object fieldValue);
    String insertQuery(T t);
    String updateQuery(T t);
    String deleteByIdQuery(Class<T> clazz, Long id);

    T initializeFields(T dataObjects, ResultSet resultSet);
    PreparedStatement fillStatement(PreparedStatement statement, T t);
}
