package application.server.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TemplateEngineConf {
    public String defaultTemplate() default "";
    public String templatesFolder() default "templates";
    public boolean templateEngineActive() default false;
}
