package application.server;

import application.server.annotations.*;
import application.server.classes.HttpServiceLocator;
import application.server.classes.MiddlewareService;
import application.server.classes.Route;
import application.server.classes.ScanType;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

@ControllerScanner(scanPath = {"application.sp02.controllers"})
@FilterScanner(scanPath = {"application.sp02.filters"})
@TemplateEngineConf(templateEngineActive = true, templatesFolder = "templates/sp02")
public class ServletDispatcher {
    private static final Logger logger = LoggerFactory.getLogger(ServletDispatcher.class);
    private ServletContextHandler handler;
    private List<Route> routesList;
    private List<Route> filtersList;
    private TemplateEngine templateEngine;

    public static class Builder {
        private final ServletDispatcher servletDispatcher = new ServletDispatcher();
        private final Comparator<Route> compareByOrder = Comparator.comparing(Route::getOrder);

        public Builder() {
            servletDispatcher.handler = new ServletContextHandler();
            servletDispatcher.routesList = new ArrayList<>();
            servletDispatcher.filtersList = new ArrayList<>();
        }

        public Builder configureTemplateEngine() throws ConfigurationException {
            if (servletDispatcher.getClass().getAnnotation(TemplateEngineConf.class).templateEngineActive())
                servletDispatcher.templateEngine = TemplateEngine.folder(servletDispatcher.getClass().getAnnotation(TemplateEngineConf.class).templatesFolder());
            return this;
        }

        public Builder configureRoutes(ScanType configureScanType) throws ConfigurationException {
            if (servletDispatcher.getClass().getAnnotation(TemplateEngineConf.class).templateEngineActive() && Objects.isNull(servletDispatcher.templateEngine))
                throw new ConfigurationException("Template engine activated but not configured. Configure template engine first.");
            return configureRoutes(configureScanType, false);
        }

        public Builder configureRoutes(ScanType configureScanType, boolean addCustomConfigured) throws ConfigurationException {
            if (configureScanType.equals(ScanType.CONFIG)) {
                configRoutesScan();
            } else {
                autoRoutesScan();
                if (addCustomConfigured)
                    configRoutesScan();
            }
            servletDispatcher.routesList.removeIf(p ->
                    servletDispatcher.routesList.stream().filter(r -> p.getUrl().equals(r.getUrl())).count() > 1
            );
            Collections.sort(servletDispatcher.routesList, compareByOrder);
            servletDispatcher.routesList.forEach(route -> {
                try {
                    Class<?> controllerClass = Class.forName(route.getHolder());
                    if (HttpServiceLocator.class.isAssignableFrom(controllerClass)) {
                        MiddlewareService middlewareService = (MiddlewareService) Arrays.stream(controllerClass.getDeclaredFields())
                                .filter(f -> f.isAnnotationPresent(WebControllerService.class))
                                .findFirst()
                                .orElseThrow(() -> new ConfigurationException("Web service not located"))
                                .getType()
                                .getConstructor()
                                .newInstance();
                        HttpServiceLocator controller = (HttpServiceLocator) controllerClass
                                .getConstructor(TemplateEngine.class, MiddlewareService.class)
                                .newInstance(servletDispatcher.templateEngine, middlewareService);
                        servletDispatcher.handler.addServlet(new ServletHolder(controller), route.getUrl());
                    } else {
                        servletDispatcher.handler.addServlet(route.getHolder(), route.getUrl());
                    }
                } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | ConfigurationException e) {
                    logger.error(String.format("Servlet not instantiated: %s", route.getHolder()));
                }

            });
            return this;
        }

        public Builder configureFilters(ScanType configureScanType) throws ConfigurationException {
            return configureFilters(configureScanType, false);
        }

        public Builder configureFilters(ScanType configureScanType, Boolean addCustomConfigured) throws ConfigurationException {
            if (configureScanType.equals(ScanType.CONFIG)) {
                configFiltersScan();
            } else {
                autoFiltersScan();
                if (addCustomConfigured)
                    configFiltersScan();
            }
            /*
            servletDispatcher.filtersList.removeIf(p ->
                    servletDispatcher.filtersList.stream().filter(r -> p.getUrl().equals(r.getUrl())).count() > 1
            );

             */
            Collections.sort(servletDispatcher.filtersList, compareByOrder);
            servletDispatcher.filtersList.forEach(filter ->
                    servletDispatcher.handler.addFilter(filter.getHolder(), filter.getUrl(), EnumSet.of(DispatcherType.REQUEST))
            );
            return this;
        }

        public ServletDispatcher start(Integer port) throws Exception {
            Server server = new Server(port);
            server.setHandler(servletDispatcher.handler);
            server.start();
            server.join();
            return servletDispatcher;
        }

        private void autoRoutesScan() {
            String[] scanPath = servletDispatcher
                    .getClass()
                    .getAnnotation(ControllerScanner.class)
                    .scanPath();
            Arrays.stream(scanPath).forEach(path -> {
                Reflections reflections = new Reflections(path);
                Set<Class<? extends Object>> controllers =
                        reflections.getTypesAnnotatedWith(WebAppController.class);
                controllers.forEach(controller -> {
                    String[] routes = controller.getAnnotation(WebAppController.class).urlPattern();
                    for (String r : routes) {
                        logger.info(String.format("New route added: %s %s",
                                controller.getCanonicalName(), r));
                        Route sdRoute = new Route(controller.getCanonicalName(), r, 0);
                        servletDispatcher.routesList.add(sdRoute);
                    }
                });
            });
        }

        private void configRoutesScan() throws ConfigurationException {
            String routesPath = servletDispatcher
                    .getClass()
                    .getAnnotation(ControllerScanner.class)
                    .configPath();
            XMLConfiguration config = new XMLConfiguration(routesPath);
            List<HierarchicalConfiguration> routes = config.configurationsAt("route");
            routes.stream().forEach(route -> {
                List<HierarchicalConfiguration> paths = route.configurationsAt("scope");
                paths.forEach(path -> {
                    Route routeSd = new Route(route.getString("controller"), path.getString("path"), 0);
                    servletDispatcher.routesList.add(routeSd);
                });
            });
        }

        private void autoFiltersScan() {
            String[] scanPath = servletDispatcher
                    .getClass()
                    .getAnnotation(FilterScanner.class)
                    .scanPath();
            Arrays.stream(scanPath).forEach(path -> {
                Reflections reflections = new Reflections(path);
                Set<Class<? extends Object>> filters =
                        reflections.getTypesAnnotatedWith(WebAppFilter.class);
                filters.forEach(filter -> {
                    String[] routes = filter.getAnnotation(WebAppFilter.class).urlPatterns();
                    Integer order = filter.getAnnotation(WebAppFilter.class).order();
                    for (String s : routes) {
                        logger.info(String.format("New filter added: %s %s",
                                filter.getCanonicalName(), s));
                        Route sdRoute = new Route(filter.getCanonicalName(), s, order);
                        servletDispatcher.filtersList.add(sdRoute);
                    }
                });
            });
        }

        private void configFiltersScan() throws ConfigurationException {
            String routesPath = servletDispatcher
                    .getClass()
                    .getAnnotation(FilterScanner.class)
                    .configPath();
            XMLConfiguration config = new XMLConfiguration(routesPath);
            List<HierarchicalConfiguration> routes = config.configurationsAt("filter");
            routes.stream().forEach(route -> {
                List<HierarchicalConfiguration> paths = route.configurationsAt("scope");
                Integer order = route.getInteger("order", 0);
                paths.forEach(path -> {
                    Route routeSd = new Route(route.getString("controller"), path.getString("path"), order);
                    servletDispatcher.filtersList.add(routeSd);
                });
            });
        }
    }
}
