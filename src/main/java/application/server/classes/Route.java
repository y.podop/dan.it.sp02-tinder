package application.server.classes;

public class Route {
    private  String holder;
    private  String url;
    private Integer order;

    public Route(String holder, String url) {
        this.holder = holder;
        this.url = url;
    }
    public Route(String holder, String url, Integer order) {
        this.holder = holder;
        this.url = url;
        this.order=order;
    }
    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
