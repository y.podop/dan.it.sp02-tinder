package application.server.classes;

import application.server.TemplateEngine;

import javax.servlet.http.HttpServlet;

public abstract class HttpServiceLocator extends HttpServlet {
    private final TemplateEngine templateEngine;
    private final MiddlewareService middlewareService;

    public HttpServiceLocator(TemplateEngine templateEngine, MiddlewareService middlewareService) {
        this.templateEngine = templateEngine;
        this.middlewareService = middlewareService;
    }

}
