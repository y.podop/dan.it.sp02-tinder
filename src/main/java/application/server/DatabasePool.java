package application.server;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DatabasePool {
    private static final Logger logger = LoggerFactory.getLogger(DatabasePool.class);
    private static final HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;
    static {
        final InputStream props = DatabasePool.class.getClassLoader().getResourceAsStream("config/db.properties");
        Properties appProps = new Properties();
        try {
            appProps.load(props);
            config.setJdbcUrl(appProps.getProperty("db_url"));
            config.setUsername(appProps.getProperty("db_username"));
            config.setPassword(appProps.getProperty("db_password"));
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("connectionTimeout", "120000");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            ds = new HikariDataSource(config);

        } catch (IOException e) {
            logger.error("DB properties loading error", e);
        }


    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
    public static void migrateSchema(){
        FluentConfiguration configuration = new FluentConfiguration()
                .dataSource(ds);
        Flyway flyway = new Flyway(configuration);
        flyway.migrate();
    }
    private DatabasePool() {
    }

}
