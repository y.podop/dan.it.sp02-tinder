<#macro mainBannerBlock>
<!-- Heading Row-->
<div class="row align-items-center my-5">
    <div class="col-lg-7">
        <img class="img-fluid rounded mb-4 mb-lg-0" src="/assets/type/img/name/dating-main.jpg" alt="..."/>
    </div>
    <div class="col-lg-5">
        <h1 class="font-weight-light">Знакомься где угодно</h1>
        <p>Часто бываешь на Бали и хочешь завести новых друзей?
            Специально для этого и придумали SP02-Tinder.
            С нашим приложением ты сможешь знакомиться не только с людьми поблизости,
            но и по всему миру.</p>
        <a class="btn btn-primary" href="/profile/list/all">Найти профайл</a>
    </div>
</div>
</#macro>