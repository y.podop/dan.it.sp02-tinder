<#macro allProfileRecord>
    <#list profiles as profile>
        <#attempt>
            <tr>
                <td width="10">
                    <div class="avatar-img">
                        <img class="img-circle"
                             src="<#if profile.profileImage??>${profile.profileImage}<#else >https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxhcCYW4QDWMOjOuUTxOd50KcJvK-rop9qE9zRltSbVS_bO-cfWA</#if>"/>  
                    </div>
                </td>
                <td class="align-middle">
                    <a href="/profile/id/${profile.id}">
                        <#if profile.firstName??>
                            ${profile.firstName}
                        </#if>
                        <#if profile.lastName??>
                            ${profile.lastName}
                        </#if>
                    </a>
                </td>
                <td class="align-middle">
                    <#if profile.profileInfo??>
                        ${profile.profileInfo}
                    </#if>
                </td>
                <td class="align-middle">
                    <#if profile.lastSession??>
                        Last Login: ${profile.lastSession}
                    </#if>
                </td>
            </tr>

            <#recover>
                <tr>
                    <td colspan="4">
                        no profiles found
                    </td>
                </tr>
        </#attempt>
    </#list>
</#macro>
<#macro likedProfileRecord>
    <#list likedProfiles as profile>
        <#attempt>
            <tr>
                <td width="10">
                    <div class="avatar-img">
                        <img class="img-circle"
                             src="<#if profile.profileImage??>${profile.profileImage}<#else >https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxhcCYW4QDWMOjOuUTxOd50KcJvK-rop9qE9zRltSbVS_bO-cfWA</#if>"/>
                    </div>
                </td>
                <td class="align-middle">
                    <a href="/profile/id/${profile.id}">
                        <#if profile.firstName??>
                            ${profile.firstName}
                        </#if>
                        <#if profile.lastName??>
                            ${profile.lastName}
                        </#if>
                    </a>
                </td>
                <td class="align-middle">
                    <#if profile.profileInfo??>
                        ${profile.profileInfo}
                    </#if>
                </td>
                <td class="align-middle">
                    <#if profile.lastSession??>
                        Last Login: ${profile.lastSession}
                    </#if>
                </td>
            </tr>
            <#recover>
                <tr>
                    <td colspan="4">
                        no profiles found
                    </td>
                </tr>
        </#attempt>
    </#list>
</#macro>
<#macro whoLikedProfileRecord>
    <#list whoLikedMe as profile>
        <#attempt>
            <tr>
                <td width="10">
                    <div class="avatar-img">
                        <img class="img-circle"
                             src="<#if profile.profileImage??>${profile.profileImage}<#else >https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxhcCYW4QDWMOjOuUTxOd50KcJvK-rop9qE9zRltSbVS_bO-cfWA</#if>"/>
                    </div>
                </td>
                <td class="align-middle">
                    <a href="/profile/id/${profile.id}">
                        <#if profile.firstName??>
                            ${profile.firstName}
                        </#if>
                        <#if profile.lastName??>
                            ${profile.lastName}
                        </#if>
                    </a>
                </td>
            </tr>
            <#recover>
                <tr>
                    <td colspan="4">
                        no profiles found
                    </td>
                </tr>
        </#attempt>
    </#list>
</#macro>