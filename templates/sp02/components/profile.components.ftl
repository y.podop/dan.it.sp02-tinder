<#macro lastRegisteredComponent>
    <!-- Content Row-->
    <div class="row">
        <#list lastRegistered as profile>
            <div class="col-md-3 mb-5">
                <div class="card h-100">
                    <div class="card-body">
                        <h5 class="card-title font-weight-normal">
                            <img class="img-circle" src="<#if profile.profileImage??>${profile.profileImage}<#else >/assets/type/img/name/profile-default.png</#if>">
                            <#if profile.firstName??>
                                ${profile.firstName}
                            </#if>
                            <#if profile.lastName??>
                                ${profile.lastName}
                            </#if>
                        </h5>
                        <p class="card-text font-weight-light">
                            <#if profile.profileInfo??>
                                ${profile.profileInfo}
                            </#if>
                        </p>
                    </div>
                    <div class="card-footer"><a class="btn btn-primary btn-sm" href="/profile/id/${profile.id}">Познакомиться</a></div>
                </div>
            </div>
        </#list>
    </div>
</#macro>