<#macro menuBlock>
    <#if !profileUserLogged>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top border-bottom">
            <div class="container">
                <img src="/assets/type/img/name/favicon.png" alt="" width="50" height="50"> <span
                        class="p-3">SP02-TINDER</span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span
                            class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="/index">Главная</a></li>
                        <li class="nav-item"><a class="nav-link" href="/profile/list/all">Профили</a></li>
                        <li class="nav-item"><a class="nav-link" href="/registration">Регистрация</a></li>
                        <li class="nav-item"><a class="nav-link" href="/login">Вход</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </#if>
    <#if profileUserLogged>
        <form action="/login/action/logout" method="post" id="logout"></form>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top border-bottom">
            <div class="container">
                <img src="/assets/type/img/name/favicon.png" alt="" width="50" height="50"> <span
                        class="p-3">SP02-TINDER</span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span
                            class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item"><a class="nav-link" href="/index">Главная</a></li>
                        <li class="nav-item"><a class="nav-link" href="/profile/list/all">Профили</a></li>
                        <li class="nav-item"><a class="nav-link" href="/profile/id/owner">Мой профиль</a></li>
                        <li class="nav-item"><a class="nav-link" onclick="document.getElementById('logout').submit();" style="cursor: pointer">Выход</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </#if>
</#macro>