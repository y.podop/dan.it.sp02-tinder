<#macro headerBlock>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <meta name="description" content=""/>
        <meta name="author" content=""/>
        <title>${pageName}</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="/assets/type/img/name/favicon.png"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="/assets/type/css/name/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/type/css/name/styles.css">
        <link rel="stylesheet" href="/assets/type/css/name/style.css">

    </head>
    <body>
</#macro>