<#if chat?? && chat.dateStart?? && chat.messages??>

    <div class="row">
        <div class="chat-main col-12">
            <div class="chat-content">
                <div class="col-md-12 chats pt-3 pl-2 pr-3 pb-3">
                    Чат начат: ${chat.dateStart}
                    <ul class="p-0">
                        <#list chat.messages as message>
                            <#if !(message.ownerId==profile.id)>
                                <li class="send-msg float-right mb-2">
                                    <p class="pt-1 pb-1 pl-2 pr-2 m-0 rounded">
                                        ${message.text}
                                    </p>
                                    <span class="receive-msg-time">Me, ${message.messageDate}</span>
                                </li>
                            </#if>
                            <#if (message.ownerId==profile.id)>
                                <li class="receive-msg float-left mb-2">
                                    <div class="sender-img">
                                        <img src="<#if profile.profileImage??>${profile.profileImage}<#else >https://robohash.org/68.186.255.198.png</#if>"
                                             class="float-left">
                                    </div>
                                    <div class="receive-msg-desc float-left ml-2">
                                        <p class="bg-white m-0 pt-1 pb-1 pl-2 pr-2 rounded">
                                            ${message.text}
                                        </p>
                                        <span class="receive-msg-time">${profile.firstName}, ${message.messageDate}</span>
                                    </div>
                                </li>
                            </#if>

                        </#list>
                    </ul>
                </div>
                <div class="col-md-12 p-2 msg-box border border-primary">
                    <form id="sendMessage" name="sendMessage" method="post">
                        <div class="row">
                            <div class="col-md-9">
                                <input type="text" name="messageText" class="border-0 w-100 h-100" placeholder="Message text"/>
                            </div>
                            <div class="col-md-3 text-right">
                                <a  class="float-right btn btn-outline-primary w-100" onclick="document.getElementById('sendMessage').submit();">Send</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</#if>