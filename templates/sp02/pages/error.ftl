<#import "../components/header.ftl" as header>
<#import "../components/menu.ftl" as menu>
<#import "../components/footer.ftl" as footer>
<@header.headerBlock></@header.headerBlock>
<@menu.menuBlock></@menu.menuBlock>
<div class="container">
    <#if warn?has_content>
        <div class="alert alert-danger my-3" role="alert">
            ${warn}
        </div>
    </#if>
</div>
<@footer.footerBlock></@footer.footerBlock>