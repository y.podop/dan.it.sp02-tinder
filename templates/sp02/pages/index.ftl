<#import "../components/header.ftl" as header>
<#import "../components/menu.ftl" as menu>
<#import "../components/banner.ftl" as banner>
<#import "../components/footer.ftl" as footer>
<#import "../components/profile.components.ftl" as profile>
<@header.headerBlock></@header.headerBlock>
<@menu.menuBlock></@menu.menuBlock>

<!-- Page Content-->
<div class="container">
    <@banner.mainBannerBlock></@banner.mainBannerBlock>
    <h3 class="font-weight-light">Последние зарегистрированные профили</h3>
    <@profile.lastRegisteredComponent></@profile.lastRegisteredComponent>
</div>
<@footer.footerBlock></@footer.footerBlock>