<#import "../components/header.ftl" as header>
<#import "../components/menu.ftl" as menu>
<#import "../components/footer.ftl" as footer>
<#import "../components/profiles.ftl" as profileRec>
<@header.headerBlock></@header.headerBlock>

<@menu.menuBlock></@menu.menuBlock>
<div class="container mb-5">
    <#if pageContent=="profileList">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs my-3">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Все</a>
            </li>
            <#if profileUserLogged>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1">Понравившиеся</a>
                </li>
            </#if>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <#assign x = profiles?size>
            <div id="home" class="container tab-pane active"><br>
                <h3>Все профайлы</h3>
                <table class="table-users table" border="0">
                    <tbody>
                    <#if (x>0)>
                        <@profileRec.allProfileRecord></@profileRec.allProfileRecord>
                    </#if>
                    <#if (x==0)>
                        Все профайлы Вам нравятся
                    </#if>
                    </tbody>
                </table>
            </div>

            <#if profileUserLogged>
                <div id="menu1" class="container tab-pane fade"><br>
                    <h3>Понравившиеся профайлы</h3>
                    <table class="table-users table" border="0">
                        <tbody>
                        <#if (likedProfiles?size>0)>
                            <@profileRec.likedProfileRecord></@profileRec.likedProfileRecord>
                        </#if>
                        <#if (likedProfiles?size==0)>
                            Никто Вам не понравился(
                        </#if>
                        </tbody>
                    </table>
                </div>
            </#if>
        </div>
    <#elseif pageContent=="profileSingle">
        <#if profile.id??>
            <#attempt>
                <form action="/profile/id/${profile.id}" method="post" id="markupSend">
                    <input type="hidden" name="markup" id="markup">
                </form>
                <div class="row">
                    <div class=" col-4 col-lg-4 col-md-12 col-sm-12 my-3">
                        <div class="card mx-1">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-lg-12 col-md-12 text-center">
                                        <img src="<#if profile.profileImage??>${profile.profileImage}<#else >https://robohash.org/68.186.255.198.png</#if>"
                                             alt="" class="mx-auto rounded-circle img-fluid">
                                        <h3 class="mb-0 text-truncated">${profile.firstName} ${profile.lastName}</h3>
                                        <h5 class="mb-0 text-truncated"><#if profile.profileInfo??>${profile.profileInfo}</#if></h5>
                                        <br>
                                    </div>
                                    <#if (profileOwner?? && !profileOwner)>
                                        <div class="col-12 col-lg-6">
                                            <button
                                                    type="button" class="btn btn-outline-danger btn-block"
                                                    onclick="document.getElementById('markup').value='DISLIKE'; document.getElementById('markupSend').submit();">
                                                <span class="fa fa-times"></span>
                                                Dislike
                                            </button>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <button
                                                    type="button" class="btn btn-outline-success btn-block"
                                                    onclick="document.getElementById('markup').value='LIKE'; document.getElementById('markupSend').submit();">
                                                <span class="fa fa-heart"></span>
                                                Like
                                            </button>
                                        </div>
                                    </#if>
                                    <!--/col-->
                                </div>
                                <!--/row-->
                            </div>
                        </div>
                        <!--/card-block-->
                    </div>
                    <div class="col-8 col-lg-8 col-md-12 col-sm-12 my-3">
                        <div class="card mx-1">
                            <div class="card-body">
                                <span class="fa fa-heart"></span>
                                Liked <#if profileStats.LIKE??> ${profileStats.LIKE}<#else > 0 </#if> times <br/>
                                <span class="fa fa-times"></span>
                                Disliked <#if profileStats.DISLIKE??> ${profileStats.DISLIKE}<#else > 0 </#if> times
                                <#if (profileOwner?? && profileOwner)>
                                    <h3>Кто отметил Вас лайком</h3>
                                    <table class="table-users table" border="0">
                                        <tbody>
                                        <#if (whoLikedMe?? && whoLikedMe?size>0)>
                                            <@profileRec.whoLikedProfileRecord></@profileRec.whoLikedProfileRecord>
                                        </#if>
                                        <#if (!whoLikedMe?? || whoLikedMe?size==0)>
                                            Никому Вы не понравились((
                                        </#if>
                                        </tbody>
                                    </table>
                                </#if>
                                <div class="col-12">
                                    <#if (!profileOwner?? || !profileOwner)>
                                        <#if ownerProfileMarkup?? && ownerProfileMarkup=="LIKE" && chat??>
                                            <#include "../components/chat.ftl">
                                        </#if>
                                    </#if>
                                    <#if !ownerProfileMarkup?? || !(ownerProfileMarkup?has_content) || !(ownerProfileMarkup=="LIKE")>
                                        <div class="card-body">
                                            Chat unavailable
                                        </div>
                                    </#if>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <#recover>
                    <div class="alert alert-info my-1" role="row">
                        No profiles found
                    </div>
            </#attempt>
        </#if>
        <#if !profile.id??>
            <div class="alert alert-info my-1" role="row">
                No profiles found
            </div>
        </#if>
    </#if>
</div>
<@footer.footerBlock></@footer.footerBlock>