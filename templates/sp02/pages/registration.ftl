<#import "../components/header.ftl" as header>
<#import "../components/footer.ftl" as footer>
<@header.headerBlock></@header.headerBlock>
<style>
    <#include "../css/style.css">
</style>
<form class="form-signin" action="/registration?redirectTo=${returnUrl}" method="post">
    <div class="card">
        <article class="card-body">
            <h4 class="card-title mb-4 mt-1">Регистрация</h4>
            <#if error?has_content>
                <div class="alert alert-danger" role="alert">
                    ${error}
                </div>
            </#if>
            <div class="form-group">
                <label>Имя</label>
                <input class="form-control" placeholder="Имя" type="text" name="firstName">
            </div>
            <div class="form-group">
                <label>Фамилия</label>
                <input class="form-control" placeholder="Фамилия" type="text" name="lastName">
            </div>
            <div class="form-group">
                <label>Ссылка на фото</label>
                <input class="form-control" placeholder="Фото" type="text" name="profileImage">
            </div>
            <div class="form-group">
                <label>Немного о себе</label>
                <input class="form-control" placeholder="Инфо" type="text" name="profileInfo">
            </div>
            <div class="form-group">
                <label>Ваш email</label>
                <input class="form-control" placeholder="Email" type="email" name="email">
            </div>
            <div class="form-group">
                <a class="float-right" href="/login">Уже есть аккаунт?</a>
                <label>Ваш пароль</label>
                <input class="form-control" placeholder="******" type="password" name="password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Зарегистрироваться</button>
            </div>
        </article>
    </div>
</form>
<@footer.footerBlock></@footer.footerBlock>