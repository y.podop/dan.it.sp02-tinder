<#import "../components/header.ftl" as header>
<#import "../components/footer.ftl" as footer>
<@header.headerBlock></@header.headerBlock>
<style>
    <#include "../css/style.css">
</style>
<form class="form-signin" action="/login/action/authorize?redirectTo=${returnUrl}" method="post">
    <div class="card">
        <article class="card-body">
            <a href="/registration" class="float-right btn btn-outline-primary">Регистрация</a>
            <h4 class="card-title mb-4 mt-1">Введите логин и пароль</h4>
            <#if error?has_content>
                <div class="alert alert-danger" role="alert">
                    ${error}
                </div>
            </#if>
            <div class="form-group">
                <label>Ваш email</label>
                <input class="form-control" placeholder="Email" type="email" name="login">
            </div>
            <div class="form-group">
                <label>Ваш пароль</label>
                <input class="form-control" placeholder="******" type="password" name="password">
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label> <input type="checkbox" name="keepLoggedIn" value="true"> Сохранить пароль </label>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Залогиниться</button>
            </div>
        </article>
    </div>
</form>
<@footer.footerBlock></@footer.footerBlock>